import express, { json, urlencoded } from "express";
import { connectDb } from "./connectdb/connectdb.js";

import { config } from "dotenv";
import { port, staticFolder } from "./config/config.js";

let expressApp = express();
config(); //then only process.env can be used in any file
expressApp.use(json());
expressApp.use(urlencoded({ extended: true }));

expressApp.use(staticFolder);

connectDb();

expressApp.listen(port, () => {
  console.log("the port is listening at 8000");
});

// admin user

// register
//login
//logout
//details
//update register (profile)//we do not update email and password
//update password for login user
//forgetpassword(reset password)
//delete
