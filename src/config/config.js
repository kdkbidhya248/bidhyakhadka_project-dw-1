export const dbUrl = process.env.DBURL;
export const port = process.env.PORT || "8000";
export const staticFolder = express.static("./public");
