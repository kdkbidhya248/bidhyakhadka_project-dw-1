import mongoose from "mongoose";
import { dbUrl } from "../config/config.js";

export let connectDb = async () => {
  mongoose.set("strictQuery", true);

  const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };

  try {
    await mongoose.connect(dbUrl, options);
    console.log("expressapp is connected to mongodb sucessfully");
  } catch (error) {
    console.log(error.message);
  }
};

//MRV:model schema then route then controller
